from ClientThread import ClientThread
from Logger import Logger
from time import strftime, localtime
from hashlib import md5
import base64
import ResponseCode
import re


class Dispatcher:

    def __init__(self):
        self.connected_users = []
        self.logger = Logger('dispatcher.log', Logger.WRITE_FILE | Logger.WRITE_CONSOLE, 'DISPATCHER')
        self.sub_directory = strftime("%Y-%m-%d %H-%M-%S", localtime())
        idle_logger = Logger('idle.log', Logger.WRITE_FILE | Logger.WRITE_CONSOLE, 'IDLE', self.sub_directory)
        self.channels = [{'name': 'idle', 'owner': 'root', 'log': idle_logger}]

    def add_new_listen_thread(self, conn, addr):
        addr = (addr[0].replace(':', ';'), addr[1])

        thread = ClientThread(self, conn)
        thread.start()

        self.logger.log('Created new thread [%s] for listening %s:%d.' % (str(thread.ident), addr[0], addr[1]))

        client_logger = Logger('%s-%d.log' % addr, Logger.WRITE_FILE | Logger.WRITE_CONSOLE, 'CLIENT', self.sub_directory)
        client = 'anon' + str(addr[1])
        self.connected_users.append({'id': thread.ident, 'conn': conn, 'addr': addr, 'channel': 'idle', 'user': client, 'log': client_logger})

    def dispatch(self, msg, from_id):
        info = self.get_info_from_id(from_id)
        addr = info['addr']
        self.log('(%s:%d) received: "%s"' % (addr[0], addr[1], msg), from_id)

        try:
            (code, parsed) = self.parse(msg)
            self.log('Decoded: "%s"' % base64.b64decode(msg), from_id)

            if code == int(ResponseCode.JOIN_CHANNEL):
                self.create_or_enter_channel(parsed, from_id)
            if code == int(ResponseCode.CHANNEL_MSG):
                channel = info['channel']
                if channel == 'idle':
                    self.send(info['conn'], ResponseCode.SYSTEM_INFO, 'You cannot send message in idle channel!')
                    self.log('You cannot send message in idle channel!', from_id)
                else:
                    self.send_to_channel(parsed, channel, from_id)
                    self.log('Sent message to "%s" channel: %s' % (channel, parsed), from_id)
            if code == int(ResponseCode.DISCONNECT):
                self.send(info['conn'], ResponseCode.DISCONNECT, "")
                info['conn'].close()
                self.log('Disconnect.', from_id)
            if code == int(ResponseCode.PRIVATE_MSG):
                split = parsed.split(' ')
                self.send_to_user(' '.join(split[1:]), split[0], from_id)
                self.log('Send private message to %s.' % split[0], from_id)
            if code == int(ResponseCode.SET_NAME):
                if len([x for x in self.connected_users if x['user'] == parsed]) != 0:
                    self.send(info['conn'], ResponseCode.SYSTEM_INFO, 'Someone is already using that name!')
                else:
                    info['user'] = parsed
                    self.send(info['conn'], ResponseCode.SYSTEM_INFO, 'Your name has been changed to %s' % parsed)
                    self.log('Changed username to %s.' % parsed, from_id)
            if code == int(ResponseCode.GET_NAME):
                self.send(info['conn'], ResponseCode.GET_NAME, info['user'])
                self.log('Asked about current username (%s).' % info['user'], from_id)
            if code == int(ResponseCode.GET_CURRENT_CHANNEL):
                self.send(info['conn'], ResponseCode.GET_CURRENT_CHANNEL, info['channel'])
                self.log('Asked about current channel (%s).' % info['channel'], from_id)

        except Exception as e:
            self.log(e[0], from_id)
            self.send(info['conn'], ResponseCode.DISCONNECT, 'Received unsupported message. You have been disconnected from server.')
            info['conn'].close()

    def close_thread(self, from_id):
        info = self.get_info_from_id(from_id)
        index = self.connected_users.index(info)
        addr = info['addr']

        self.logger.log('Closed thread [%d] for %s:%d.' % (info['id'], addr[0], addr[1]))
        del self.connected_users[index]

    def send_to_user(self, msg, user, from_id):
        pass
        infoFrom = self.get_info_from_id(from_id)
        infoTo = self.get_info_from_name(user)
        if infoTo is None:
            self.send(infoFrom['conn'], ResponseCode.SYSTEM_INFO, "Given username doesnt exist.")
            self.log('Tried to send PM to not existing person (%s).' % user, from_id)
            return

        msg_to_send = strftime("[%H:%M:%S] %%s %%s: %%s", localtime())

        self.send(infoFrom['conn'], ResponseCode.PRIVATE_MSG, msg_to_send % ('TO', user, msg))
        self.send(infoTo['conn'], ResponseCode.PRIVATE_MSG, msg_to_send % ('FROM', infoFrom['user'], msg))

    def send_to_channel(self, msg, channel, from_id, with_sender=True, with_name=True):
        client_from = self.get_info_from_id(from_id)
        msg_to_send = strftime("[%H:%M:%S] %%s", localtime())
        if with_name:
            msg_to_send = msg_to_send % client_from['user']+': '+msg
        else:
            msg_to_send = msg_to_send % msg

        for client_info in [x for x in self.connected_users if x['channel'] == channel and (with_sender or x['id'] != from_id)]:
            addr = client_info['addr']
            self.send(client_info['conn'], ResponseCode.CHANNEL_MSG, msg_to_send)
            client_info['log'].log('(%s:%d) [%s] send: "%s"' % (addr[0], addr[1], channel, base64.b64encode(msg)))
        self.get_channel_from_name(channel)['log'].log(msg_to_send)

    def create_or_enter_channel(self, channel_name, from_id):
        client_info = self.get_info_from_id(from_id)
        channel = self.get_channel_from_name(channel_name)

        if channel is None and channel_name != 'idle':
            channel_logger = Logger('channel_%s.log' % channel_name, Logger.WRITE_FILE | Logger.WRITE_CONSOLE, channel_name, self.sub_directory)
            channel_logger.log('Channel created by %s.' % client_info['user'])
            channel = {'name': channel_name, 'owner': client_info['id'], 'log': channel_logger}
            self.channels.append(channel)
            self.send(client_info['conn'], ResponseCode.SYSTEM_INFO, 'You have created "%s" channel.' % channel_name)
            client_info['log'].log('Created "%s" channel.' % channel_name)
            client_info['channel'] = channel_name
        else:
            if client_info['channel'] != 'idle':
                self.send(client_info['conn'], ResponseCode.SYSTEM_INFO, 'You have left "%s" channel.' % client_info['channel'])
                self.send_to_channel('%s left this channel.' % client_info['user'], client_info['channel'], from_id)
                client_info['log'].log('Left "%s" channel.' % channel_name)

            self.send(client_info['conn'], ResponseCode.SYSTEM_INFO, 'You have joined "%s" channel.' % channel_name)
            self.send_to_channel('%s joined this channel.' % client_info['user'], channel_name, from_id, with_name=False)
            client_info['log'].log('Joined "%s" channel.' % channel_name)
            client_info['channel'] = channel_name

    def disconnect(self, msg, from_id, silent=False):
        info = self.get_info_from_id(from_id)
        if info['channel'] != 'idle':
            self.send_to_channel('%s disconnected or lost connection.' % info['user'], info['channel'], from_id, with_sender=False, with_name=False)

        info['conn'].close()
        if not silent:
            info['log'].log(msg % info['addr'])

    def log(self, msg, from_id):
        info = self.get_info_from_id(from_id)
        info['log'].log(msg)

    def get_info_from_id(self, from_id):
        return (x for x in self.connected_users if x['id'] == from_id).next()

    def get_info_from_name(self, name):
        names = [x for x in self.connected_users if x['user'] == name]
        if not names:
            return None
        return names[0]

    def get_channel_from_name(self, channel):
        channels = [x for x in self.channels if x['name'] == channel]

        if len(channels) == 0:
            return None
        return channels[0]

    @staticmethod
    def send(conn, response_code, msg):
        hash = md5(msg).hexdigest()[:6]
        pack = '{%s:%s:%s}\x01' % (response_code, hash, msg)

        conn.sendall(pack)

    @staticmethod
    def parse(msg):
        msg = base64.b64decode(msg)

        if msg[0] != '{' or msg[-1] != '}':
            raise Exception('Invalid message frames.')
        if msg[3] != ':' or msg[10] != ':':
            raise Exception('Invalid separators.')

        code = msg[1:3]
        if re.match(r'\d\d', code) is None:
            raise Exception('Given code is not integer.')
        code = int(code)
        if code >= ResponseCode.RESPONSES:
            raise Exception('Unsupported ResponseCode.')

        hash = msg[4:10]
        if re.match(r'[\dabcdef]{6}', hash) is None:
            raise Exception('Given hash does not have supported format.')

        data = msg[11:-1]
        if md5(data).hexdigest()[:6] != hash:
            raise Exception('Data has been modified!')

        if ResponseCode.is_valid_data_format(code, data):
            return code, data

        raise Exception('Received data did not pass response code validation.')
