import unittest
import socket
from hashlib import md5


class TestIPProtocols(unittest.TestCase):

    def test_ipv4_connection(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('127.0.0.1', 1337))

        welcome_msg = sock.recv(1024)

        self.assertTrue('Hey user!' in welcome_msg)

    def test_ipv6_connection(self):
        sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
        sock.connect(('::1', 1337))

        welcome_msg = sock.recv(1024)

        self.assertTrue('Hey user!' in welcome_msg)


class TestLongMessages(unittest.TestCase):
    def setUp(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect(('127.0.0.1', 1337))

        self.sock.recv(1024)  # welcome
        self.sock.sendall('{03:098f6b:test}\x01')  # create channel
        self.sock.recv(1024)  # create channel response

    def test_long_message(self):
        code = '01'
        msg = 'A'*128000
        hash = md5(msg).hexdigest()[:6]

        self.sock.sendall('{%s:%s:%s}\x01' % (code, hash, msg))
        response = self.receive_all()

        self.assertTrue(msg in response)

    def receive_all(self):
        msg = ''

        while True:
            if msg == '':
                chunk = self.sock.recv(1024)
            else:
                self.sock.settimeout(1)
                chunk = self.sock.recv(1024)

            msg += chunk

            if chunk[-1] == '\x01':
                break

        self.sock.settimeout(None)
        return msg[:-1]




if __name__ == '__main__':
    unittest.main()
