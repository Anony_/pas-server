from Logger import Logger
from Dispatcher import Dispatcher
from ListenThread import Listen
import socket


if __name__ == '__main__':
    server_log = Logger('server.log', Logger.WRITE_FILE | Logger.WRITE_CONSOLE, 'SERVER')
    server_log.log('Application start.')
    dispatcher = Dispatcher()

    listen_ipv4 = Listen(dispatcher, socket.AF_INET, server_log)
    listen_ipv6 = Listen(dispatcher, socket.AF_INET6, server_log)

    listen_ipv4.start()
    listen_ipv6.start()
