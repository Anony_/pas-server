from threading import Thread
import socket
import base64


class ClientThread(Thread):

    def __init__(self, handler, conn):
        Thread.__init__(self)

        self.handler = handler
        self.conn = conn
        self.timeout = 1

    def run(self):
        try:
            while True:
                msg = self.recv_all()

                self.handler.dispatch(base64.b64encode(msg), self.ident)

        except socket.error:
            self.handler.disconnect("(%s:%d) disconnected or lost connection.", self.ident)

        finally:
            self.handler.close_thread(self.ident)

    def recv_all(self):
        try:
            msg = ''

            while True:
                if msg == '':
                    chunk = self.conn.recv(1024)
                else:
                    self.conn.settimeout(self.timeout)
                    chunk = self.conn.recv(1024)

                msg += chunk

                if chunk[-1] == '\x01':
                    break

            self.conn.settimeout(None)
            return msg[:-1]

        except:
            raise socket.error
