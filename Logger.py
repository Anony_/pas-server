from time import strftime, localtime
import Configuration
import os


class Logger:
    WRITE_FILE = 1
    WRITE_CONSOLE = 2

    def __init__(self, file_name, flags, friendly_name='', sub_directory=None):
        log_directory = Configuration.get_value('logger', 'log_directory')

        self.file_path = log_directory
        self.flags = flags
        self.friendly_name = '[%s] ' % friendly_name

        if not os.path.isdir(log_directory):
            os.makedirs(log_directory)

        if sub_directory is not None:
            if not os.path.isdir(log_directory+'/'+sub_directory):
                os.makedirs(log_directory+'/'+sub_directory)
            self.file_path += '/'+sub_directory

        self.file_path += '/'+file_name

        if flags & self.WRITE_FILE:
            self.log_file('#'*40)

    def log(self, msg):
        msg_template = strftime("%Y-%m-%d %H:%M:%S %%s", localtime())

        if self.flags & self.WRITE_FILE:
            self.log_file(msg_template % msg)

        if self.flags & self.WRITE_CONSOLE:
            self.log_console(msg_template % self.friendly_name + msg)

    def log_file(self, msg):
        with open(self.file_path, 'a') as f:
            f.write(msg + '\n')

    @staticmethod
    def log_console(msg):
        print msg
