from threading import Thread
import Configuration
import socket


class Listen(Thread):
    def __init__(self, dispatcher, ip_prot_raw, server_log):
        Thread.__init__(self)

        self.dispatcher = dispatcher
        self.ip_prot_raw = ip_prot_raw
        self.ip_prot = 'ipv4' if ip_prot_raw == 2 else 'ipv6'
        self.server_log = server_log
        self.sock = None

    def run(self):
        try:
            self.sock = self.bind_socket()
            self.server_log.log('[%s] Socket bind successful.' % self.ip_prot)
            max_conn = self.set_max_conn()
            self.server_log.log('[%s] Maximum number of queued connections set to %s.' % (self.ip_prot, max_conn))

            while True:
                (conn, addr) = self.accept_incoming_connection()
                self.dispatcher.add_new_listen_thread(conn, addr)
                self.server_log.log('[%s] Received connection from %s on port %d.' % (self.ip_prot, addr[0], addr[1]))
                self.send_welcome_message(conn)

        except Exception as e:
            for msg in e:
                self.server_log.log(str(msg))

        finally:
            if 'sock' in locals():
                self.sock.close()

                self.server_log.log('[%s] Application exit.' % self.ip_prot)

        exit(1)

    @staticmethod
    def send_welcome_message(conn):
        msg = 'Hey user! This is final application for programming of network applications.'
        conn.sendall(msg)

    def set_max_conn(self):
        try:
            max_conn = Configuration.get_value('server', 'max_connections')

            self.sock.listen(int(max_conn))
        except ValueError:
            raise Exception('Max connections could not be converted to integer. Given: "%s"' % max_conn)

        return max_conn

    def accept_incoming_connection(self):
        (conn, addr) = self.sock.accept()

        return conn, addr

    def bind_socket(self):
        try:
            sock = socket.socket(self.ip_prot_raw, socket.SOCK_STREAM)

            listen_ip = Configuration.get_value('server', 'listen_'+self.ip_prot)
            listen_port = Configuration.get_value('server', 'listen_port')

            sock.bind((listen_ip, int(listen_port)))
        except socket.error as msg:
            raise Exception('Failed to bind. Error code: %s Message: %s' % (str(msg[0]), msg[1]))
        except ValueError:
            raise Exception('Listen port could not be converted to integer. Given: "%s"' % listen_port)
        except OverflowError:
            raise Exception('Listen port must be in range 0~65535. Given: "%s"' % listen_port)

        return sock