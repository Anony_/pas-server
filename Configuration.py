import ConfigParser

CONFIG_FILENAME = 'configuration.ini'


def get_config():
    config = ConfigParser.ConfigParser()
    config.read(CONFIG_FILENAME)

    return config


def get_section(section_name):
    config = get_config()

    if not config.has_section(section_name):
        raise Exception('Could not find "%s" section name in "%s" config file.' % (section_name, CONFIG_FILENAME))

    return dict(config.items(section_name))


def get_value(section_name, parameter_name):
    section = get_section(section_name)

    if parameter_name not in section:
        raise Exception('Could not find "%s" parameter name in "%s" section.' % (parameter_name, section_name),
                        'Section dump: "%s".' % str(section))

    return section[parameter_name]
