import re

RESPONSES = 8

SYSTEM_INFO = '00'
CHANNEL_MSG = '01'
PRIVATE_MSG = '02'
JOIN_CHANNEL = '03'
DISCONNECT = '04'
SET_NAME = '05'
GET_NAME = '06'
GET_CURRENT_CHANNEL = '07'


def is_valid_data_format(code, data):
    msg = r'[0-9a-zA-Z,.:;\'"/\\<>\[\]{}|=+-_!#$%^&*()`~ ]+\Z'

    if code == int(SYSTEM_INFO):
        return re.match('^'+msg, data) is not None
    if code == int(CHANNEL_MSG):
        return re.match('^'+msg, data) is not None
    if code == int(PRIVATE_MSG):
        return re.match(r'^[a-zA-Z0-9]+ '+msg, data) is not None
    if code == int(JOIN_CHANNEL):
        return re.match(r'^[a-zA-Z]+\Z', data) is not None
    if code == int(SET_NAME):
        return re.match(r'^[a-zA-Z0-9]+\Z', data) is not None
    if code == int(GET_NAME) or code == int(GET_CURRENT_CHANNEL) or code == int(DISCONNECT):
        return True

    raise Exception('Unsupported code.')
